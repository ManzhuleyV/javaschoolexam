package com.tsystems.javaschool.tasks.calculator;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Calculator {
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        String result= calculate(statement);
        //round more than 5 digits after the dot
        return !(result == null) && (result.matches("\\d*\\.\\d{5,}")) ? String.format(Locale.ROOT, "%.4f", Double.parseDouble(result)) : result;
    }
    /**
     * Checks if it is possible to calculate expression.
     *
     * @param str expression as string
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    public boolean checkStr(String str) {
        return  //checking for null
                str != null &&
                // checking for symbols and digits
                str.matches("[\\(-9&&[^\\,]]+") &&
                //checking for doubled symbols exc second minus
                ((!str.matches(".*[\\*-\\/]+[\\*\\+\\.\\/]+.*") || str.matches(".*\\-{2}+.*")) & !str.contains("---")) &&
                //checking for quantity of parentheses
                ((str + "\0").split("\\(").length - 1) == ((str + "\0").split("\\)").length - 1) &&
                //checking for first parentheses
                (str.indexOf("(") <= str.indexOf(")")) &&
                //checking for symbols nearby parentheses
                ((!str.contains("(")) | (("+" + str).matches(".*\\D+\\([\\d\\(\\)]+.*") && (str + "+").matches(".*[\\d\\(\\)]+\\)\\D+.*")));
    }

    /**
     * After check to possible to calculate expression, searches for an expression in the deepest parentheses.
     * Expressions before parentheses are written in startStr, after - endStr, that in parentheses - midStr.
     *
     * As long as the expression contains "*" or "/", multiplication and division are performed.
     * Then addition and subtraction.
     *
     * Further, the expression is again assembled from parts,
     * and if there are no mathematical symbols, the result is returned, else this method is called recursively.
     *
     * @param str expression as string
     * @return string value containing result of evaluation (but maybe many decimal places)
     *         or null if statement is invalid
     */
    public String calculate(String str){
        if (checkStr(str)==false) return null;
        String startStr, midStr, endStr;
        midStr=str;
        startStr=endStr="";

        if (str.contains("(")) {
            Pattern pattern = Pattern.compile("(.+?)\\(([\\*-9]+)\\)(.+)");
            Matcher matcher=pattern.matcher(" "+str+" ");
            matcher.find();
            startStr = matcher.group(1).replace(" ","");
            midStr=matcher.group(2);
            endStr=matcher.group(3).replace(" ","");
        }

        while (midStr.contains("*")||midStr.contains("/")) {
            //checking which symbols is found earlier, if "*" and it is or no "/" then do multiplication
            if (((midStr.indexOf("*") < midStr.indexOf("/")) & midStr.contains("*")) || (!midStr.contains("/")))
                midStr = compress(midStr, Symbol.MULTIPLY);
            else {
                midStr = compress(midStr, Symbol.DIVIDE);
                //because method compress may return "Infinity" by dividing the zero but it is forbidden, returns null
                if (midStr.equals("Infinity"))
                    return null;
            }
        }
        while (midStr.contains("+")||midStr.contains("-")) {
            //checking which symbols is found earlier (not considering the possible first minus), if "+" then do addition
            if (midStr.contains("+")&((midStr.indexOf("+") < midStr.lastIndexOf("-"))||(midStr.lastIndexOf("-")<1)))
                midStr = compress(midStr, Symbol.PLUS);
            //checking if doing subtraction, but this is only one "-", and it is first
            else if ((midStr.indexOf("-")==midStr.lastIndexOf("-"))&&(midStr.indexOf("-")==0))
                break;
            else
                midStr = compress(midStr, Symbol.MINUS);
        }

        str=startStr+midStr+endStr;

        //checking if this is only one "-", and it is first
        if (str.matches("-[\\d\\.]*")) return str;

        //checking if there are any mathematical symbols, do this method again
        if (str.matches(".+?[\\(-\\/&&[^\\.]]+.*")) str=calculate(str);

        return str;
    }
    /**
     * Depending on the required operation, one of the switches case is performed.
     * Expression is split into four parts.
     * Second and third contain the numbers with which needed to perform the operation.
     * First and fourth are before and after numbers.
     * After the necessary operation, the expressions are collected again.
     *
     *
     * @param str expression as string
     * @param s Enum of mathematical symbols
     * @return expression as string with one completed operation
     */
    private String compress(String str, Symbol s) {
        String startStr, endStr;
        Double result;
        Pattern pattern = Pattern.compile(s.regEx);
        Matcher matcher = pattern.matcher((" " + str + " "));
        matcher.find();
        startStr = matcher.group(1);
        endStr = matcher.group(4);

        switch (s) {
            case MULTIPLY: {
               result = Double.parseDouble(matcher.group(2)) * Double.parseDouble(matcher.group(3));
                break;
            }
            case DIVIDE: {
               result = Double.parseDouble(matcher.group(2)) / Double.parseDouble(matcher.group(3));
                break;
            }
            case PLUS: {
                result = Double.parseDouble(matcher.group(2)) + Double.parseDouble(matcher.group(3));
                break;
            }
            case MINUS: {
             result = Double.parseDouble(matcher.group(2)) - Double.parseDouble(matcher.group(3));
                break;
            }
            default:
                throw new IllegalStateException("Unexpected value: " + s);
        }
        //checking if result is integer
        return result % 1 == 0 ? (startStr + String.format("%.0f", result) + endStr).replace(" ", "") : (startStr + result + endStr).replace(" ", "");

    }
    /**
     * Enum for possibility mathematical symbols
     */
    enum Symbol{
        PLUS ("(\\s)(-?\\d+\\.?\\d*)\\+(-?\\d+\\.?\\d*)(.*+)"),
        MINUS ("(\\s)(-?\\d+\\.?\\d*)\\-(-?\\d+\\.?\\d*)(.*+)"),
        MULTIPLY ("(.*?[\\+]?)(-?\\d+\\.?\\d*)\\*(-?\\d+\\.?\\d*)(.*+)"),
        DIVIDE ("(.*?[\\+]?)(-?\\d+\\.?\\d*)\\/(-?\\d+\\.?\\d*)(.*+)");

        private String regEx;
        Symbol(String regEx) {
            this.regEx=regEx;
        }
    }
}
