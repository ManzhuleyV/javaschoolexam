package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {
    /**
     * Builds a pyramid with sorted values (with minimum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        int[][] pyramid = checkSize(inputNumbers);
        try{
            Collections.sort(inputNumbers);
        }
        catch (NullPointerException e){
            throw new CannotBuildPyramidException();
        }

        return fillPyramid(pyramid,inputNumbers);
    }
    /**
     * Checking quantity of elements in input list and creating an array of size [height][width] filled with "0"
     *
     * @param list to be used in the pyramid
     * @return 2d array with "0" inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    private int[][] checkSize(List<Integer> list){
        int quantity,height,width;
        height=width=quantity=1;

        while (quantity<list.size()){
            //quantity of elements should be larger by incremented height
            quantity+=++height;
            //width of new array should be 2 elements larger ("0"+new int)
            width+=2;
            //if quantity overflowed
            if (quantity<0) throw new CannotBuildPyramidException();
        }
        if (quantity>list.size()) throw new CannotBuildPyramidException();

        int[][] pyramid = new int[height][width];

        for (int[] i : pyramid)
            for (int j: i)
                j = 0;

        return pyramid;
    }
    /**
     * Fill pyramid with elements from list.
     *
     * n -  ordinal number of inserted list element,
     * i -  each row of pyramid,
     * j -  different between center of pyramid (width/2) and inserted element,
     *      incremented each time by two ("0" and new element)     *
     *
     * @param pyramid 2d array with "0" inside
     * @param list to be used in the pyramid
     * @return 2d array with pyramid of int inside
     */
    private int[][] fillPyramid (int[][] pyramid, List<Integer> list){
        int height=pyramid.length;
        int width=pyramid[0].length;
        int n=0;

        for (int i=0;i<height;i++)
            for (int j=-i;j<=i;j+=2)
                pyramid[i][width/2+j]=list.get(n++);

        return pyramid;
    }
}
