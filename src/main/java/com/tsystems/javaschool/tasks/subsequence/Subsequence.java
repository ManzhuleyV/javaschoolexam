package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {
    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        //checking that list is not null
        if ((x==null)||(y==null)) throw new IllegalArgumentException();

        int s=0;
        int matches =0;

        //iterating all elements from x list
        for (Object o : x)
            //iterating position for elements from y list starting from s
            for (int j = s; j < y.size(); j++)
                //if element is founded - stop y-iteration
                if (o == y.get(j)) {
                    //next y-iteration will be started from position of founded element
                    s = j;
                    matches++;
                    break;
                }

        return matches==x.size();
    }
}
